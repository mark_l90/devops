package simpleCalc;

import java.util.Scanner;

public class Calculator {
	
	public static void main (String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
				
		float fnum;
		float snum;
		float ans=0;
		float op;
		
		System.out.print("first number:");
		fnum=scan.nextFloat();
		
		System.out.println("operator:  1=add   2=subtract  3=multiply  4=divide");
		op=scan.nextFloat();
		
		System.out.print("second number: ");
		snum=scan.nextFloat();
		
		System.out.print("the answer is: ");
		
		if (op == 1) {
			ans = fnum + snum;	
		}
		if (op == 2) {
			ans = fnum - snum;	
		}
		if (op == 3) {
			ans = fnum * snum;	
		}
		if (op == 4) {
			ans = fnum / snum;	
		}
		
		System.out.print(ans);
	}

}

